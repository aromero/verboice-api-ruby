Gem::Specification.new do |s|
  s.name = %q{verboice}
  s.version = "0.2.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["InSTEDD"]
  s.date = %q{2011-11-24}
  s.description = %q{Access the Verboice API in ruby.}
  s.email = %q{aromero@manas.com.ar}
  s.homepage = %q{https://bitbucket.org/instedd/verboice-api-ruby}
  s.require_paths = ["lib"]
  s.files = [
    "lib/verboice.rb",
    "lib/verboice/exception.rb"
  ]
  s.rubygems_version = %q{1.3.6}
  s.summary = %q{Access the Verboice API in ruby}

  if s.respond_to? :specification_version then
    current_version = Gem::Specification::CURRENT_SPECIFICATION_VERSION
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<rest-client>, [">= 0"])
      s.add_runtime_dependency(%q<json>, [">= 0"])
    else
      s.add_dependency(%q<rest-client>, [">= 0"])
      s.add_dependency(%q<json>, [">= 0"])
    end
  else
    s.add_dependency(%q<rest-client>, [">= 0"])
  end
end

