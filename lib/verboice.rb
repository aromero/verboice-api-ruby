# Provides access to the Verboice Public API.
#
# === Install
#
#   gem install verboice
#
# === Example
#
#   api = Verboice.new 'service_url', 'account_name', 'account_password', 'channel'
require 'rubygems'
require 'net/http'
require 'json'
require 'rest_client'
require 'cgi'
require 'time'
require File.expand_path('../verboice/exception', __FILE__)

# Provides access to the Verboice Public API.
class Verboice
  # Creates an account-authenticated Verboice api access.
  def initialize(url, account, password, default_channel = nil)
    @url = url
    @account = account
    @password = password
    @default_channel = default_channel
    @options = {
      :user => account,
      :password => password,
      :headers => {:content_type => 'application/json'},
    }
  end

  def call address, options = {}
    args = {:address => address}
    
    args[:channel] = options[:channel] || @default_channel
    args[:not_before] = options[:not_before].iso8601 if options[:not_before]
    args[:queue] = options[:queue] if options[:queue]

    if options[:flow]
      post "/api/call?#{self.class.to_query args}", options[:flow] do |response, error|
        raise Verboice::Exception.new error.message if error
        JSON.parse response.body
      end
    else
      args[:callback_url] = options[:callback_url] if options[:callback_url]
      get_json "/api/call?#{self.class.to_query args}"
    end
  end

  def call_state id
    get_json "/api/call/#{id}/state"
  end

  def create_channel(channel)
    post "/api/channels.json", channel.to_json do |response, error|
      handle_channel_error error if error

      channel = JSON.parse response.body
      with_indifferent_access channel
    end
  end

  # Deletes a chnanel given its name.
  #
  # Raises Verboice::Exception if something goes wrong.
  def delete_channel(name)
    delete "/api/channels/#{name}" do |response, error|
      raise Verboice::Exception.new error.message if error

      response
    end
  end
  
  def call_queues
    get_json "/api/call_queues.json"
  end
  
  def call_queue(name)
    get_json "/api/call_queues/#{name}.json"
  end
  
  def create_call_queue(call_queue)
    post "/api/call_queues", call_queue.to_json do |response, error|
      raise Verboice::Exception.new error.message if error
      response
    end
  end
  
  def update_call_queue(name, call_queue)
    put "/api/call_queues/#{name}", call_queue.to_json do |response, error|
      raise Verboice::Exception.new error.message if error
      response
    end
  end
  
  def delete_call_queue(name)
    delete "/api/call_queues/#{name}" do |response, error|
      raise Verboice::Exception.new error.message if error
      response
    end
  end

  private

  def get(path)
    resource = RestClient::Resource.new @url, @options
    resource = resource[path].get
    yield resource, nil
  rescue => ex
    yield nil, ex
  end

  def get_json(path)
    get(path) do |response, error|
      raise Verboice::Exception.new error.message if error

      elem = JSON.parse response.body
      elem.map! { |x| with_indifferent_access x } if elem.is_a? Array
      elem
    end
  end

  def post(path, data)
    resource = RestClient::Resource.new @url, @options
    resource = resource[path].post(data)
    yield resource, nil
  rescue => ex
    yield nil, ex
  end
  
  def put(path, data)
    resource = RestClient::Resource.new @url, @options
    resource = resource[path].put(data)
    yield resource, nil
  rescue => ex
    yield nil, ex
  end

  def delete(path)
    resource = RestClient::Resource.new @url, @options
    resource = resource[path].delete
    yield resource, nil
  rescue => ex
    yield nil, ex
  end

  def self.to_query(hash)
    query = ''
    first = true
    hash.each do |key, value|
      query << '&' unless first
      query << key.to_s
      query << '='
      query << CGI.escape(value.to_s)
      first = false
    end
    query
  end

  def handle_channel_error(error)
    if error.is_a? RestClient::BadRequest
      response = JSON.parse error.response.body
      raise Verboice::Exception.new response['summary'], Hash[response['properties'].map {|e| [e.keys[0], e.values[0]]}]
    else
      raise Verboice::Exception.new error.message
    end
  end

  def with_indifferent_access(hash)
    hash = hash.with_indifferent_access if hash.respond_to? :with_indifferent_access
    hash
  end
end
